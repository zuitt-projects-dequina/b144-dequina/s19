// Javascript | ES6 Updates


const getCube = 2 ** 3;

	console.log(`The cube of 2 is ${getCube}`);

const address = ['258', 'Washington Ave NW', 'California', '90011'];

	const [houseNumber, streetName, stateName, zipCode] = address;

	console.log(`I live at ${houseNumber} ${streetName}, ${stateName} ${zipCode}`);



const animal = {
	name: 'Lolong',
	type: 'salt water crocodile',
	weight: '1075',
	size: '20ft 3in'
}

	const {name, type, weight, size} = animal;

	console.log(`${name} is a ${type}. It weighed ${weight}kgs with a measurement of ${size}.`);


const num = [1, 2, 3, 4, 5];

	num.forEach((number) => console.log(number));




const reduceNumber = (value1, value2) => value1 + value2;

	console.log(num.reduce(reduceNumber)); 






class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

	const myDog = new Dog('Aston', 15, 'French Bulldog');
		
		console.log(myDog);

		